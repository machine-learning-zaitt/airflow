## README
O airflow é uma ferramenta de organização de fluxo de trabalho que é amplamente utilizada como uma ferramenta de ETL.

## Estrutura
![Project Infra](images/infra.png)

### Componentes

*  O P é uma **base Postgres** que funciona no **RDS da AWS**. Essa base é onde ficam guardados os **METADADOS** utilizados pelo Airflow. Entre os metadados estão as horas das execuções, as conexões, os usuários.  <br>A configuração dessa conexão é feita por meio de váriavel de ambiente e está no arquivo `env.list`. <br>**Importante: Os Logs não fazem parte dos metadados e são guardados no S3. NÃO É POSSIVEL RECONSTRUIR AS EXECUÇÕES A PARTIR DOS LOGS!**


*  O CI/CD está em uma instancia do EC2 na amazon sendo responsavel por executar o pipeline de DevOps. <br>A configuração do pipeline está no arquivo `.gitlab-ci.yml`. <br> A configuração de conexão pode ser encontrado no ambiente do gitlab dentro de `Settings -> CI/CD -> Runners -> Specific Runners`

*  Os sources são as fontes das quais serão obtido os dados. <br> A configuração é feita por váriaveis de ambiente e se encontram no `env.list` do projeto.

*  O Airflow está em uma instancia da AWS e na seção **Instação** será explicado como fazer a instalação e configuração.



## Instalação e inicialização

### AWS

1. Pelo console da AWS crie uma instancia na aws com a imagem do ubuntu 18.

2. Pelo console da AWS abra a conexão da instancia na porta 8080.

3. Acesse a instância por ssh: `ssh -i "PEM-FILE" <INSTANCE>.sa-east-1.compute.amazonaws.com`

4. Gere uma key ssh: `ssh-keygen -t rsa -b 4096 -C "email@example.com"`

5. No projeto do gitlab Adicione a key: Foto do profile -> Settings -> SSH Keys

6. Na instancia: `git clone <This project>` (copie por ssh senão vai zuar toda parada)

7. `cd airflow; bash install.sh`

8. `docker run -v /home/ubuntu/airflow/dags:/usr/local/airflow/dags -v /home/ubuntu/airflow/plugins:/usr/local/airflow/plugins --env-file /home/ubuntu/airflow/env.list -d -p 8080:8080 registry.gitlab.com/machine-learning-zaitt/airflow webserver`

9. Acesse de qualquer lugar pelo endereço: `ipDaInstancia:8080`

### Local

1. Faça os passos da instalação na AWS a partir do 4 até passo 7.
2. `docker-compose -f docker-compose-LocalExecutor.yml up -d`
3. Acesse de qualquer lugar pelo endereço: `localhost:8080`

## Primeira inicialização do Postgres (Metadados)

A primeira vez em que o banco remoto é inicializado é necessário criar o primeiro usuário para que seja possível logar no airflow.
Isso pode ser feito acessando a instância remota na qual o airflow **esta rodando** (ou localmente) executando o comando:

`bash createUser.sh`

O primeiro usuário será criado com as credenciais contidas no `createUser.sh`

## Configuração do CI/CD
Depois de realizada a instalação na AWS siga os passos seguintes para configurar o CI/CD:
1. Alterar os ips de conexão **ssh** encontrados no `.gitlab-ci.yml`.
2. É necessario utilizar uma PEM para se conectar, essa PEM foi configurada no ambiente do gitlab e fica em: `Settings -> CI/CD -> Runners -> Variables`