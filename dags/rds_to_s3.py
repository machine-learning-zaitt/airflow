"""
Code that goes along with the Airflow located at:
http://airflow.readthedocs.org/en/latest/tutorial.html
"""
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.operators.my_first_plugin import PostgresExtractor, S3DataWriter, Verifier
from airflow.operators.dummy_operator import DummyOperator
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from airflow.operators.postgres_operator import PostgresOperator
import os

environment = os.environ["ENVIRONMENT"]

default_args = {
    "owner": "airflow",
    "depends_on_past": True,
    "start_date": datetime(2020, 1, 31),
    "email": ["airflow@airflow.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 1,
    "retry_delay": timedelta(minutes=5),
    "max_active_runs": 1,
    "concurrency": 1,
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}
if environment == 'dev':
  default_args["start_date"] = datetime(2020, 2, 3)
elif environment == 'homo':
  default_args["start_date"] = datetime(2020, 2, 1)
elif environment == 'prod':
  default_args["start_date"] = datetime(2020, 2, 2)
else:
  raise ValueError('Wrong environment name')

dag = DAG("rds_to_s3", default_args=default_args,user_defined_macros=dict(env=environment),schedule_interval=timedelta(3))

START = DummyOperator(
    task_id='Start',
    dag=dag
)

END = DummyOperator(
    task_id='End',
    dag=dag
)
t1 = PostgresExtractor(
  task_id='Postgres_extractor_sales',
  sql_conn_id='rds_zaitt_postgres',
  schema='zaitt3',
  query='SELECT * from public.sales',
  run_time="{{ ds }}",
  last_run_time ="{{ prev_ds }}",
  wait_for_downstream=True,
  dag=dag)

t2 = S3DataWriter(
  task_id='Metadata_extractor_sales',
  key='s3://ml-zaitt/{{ env }}/',
  metaData_name='columns_name_type',
  wait_for_downstream=True,
  dag=dag
)

t9 = Verifier(
  task_id='Verifier_sales',
  last_run_time="{{ prev_ds }}",
  start_date=default_args['start_date'],
  wait_for_downstream=True,
  dag=dag
)

t4 = S3DataWriter(
  task_id='Data_extractor_sales',
  key='s3://ml-zaitt/{{ env }}/',
  metaData_name='data',
  wait_for_downstream=True,
  dag=dag
)

t5 = PostgresExtractor(
  task_id='Postgres_extractor_users',
  sql_conn_id='rds_zaitt_postgres',
  schema='zaitt3',
  query='SELECT * from public.users',
  run_time="{{ ds }}",
  last_run_time ="{{ prev_ds }}",
  wait_for_downstream=True,
  dag=dag)


START >> t1 >> t2 >> t9 >> t4 >> END
START >> t5 >> t2 >> t9 >> t4 >> END
