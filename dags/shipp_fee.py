"""
Code that goes along with the Airflow located at:
http://airflow.readthedocs.org/en/latest/tutorial.html
"""
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.operators.shipp_fee_plugin import MySqlExtractor, DataValidation, And
from airflow.operators.dummy_operator import DummyOperator
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from airflow.operators.postgres_operator import PostgresOperator
import os

environment = os.environ["ENVIRONMENT"]

default_args = {
    "owner": "airflow",
    "depends_on_past": True,
    "start_date": datetime(2020, 1, 31),
    "email": ["airflow@airflow.com"],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 10,
    "retry_delay": timedelta(seconds=30),
    "max_active_runs": 1,
    "concurrency": 1,
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}

if environment == 'dev':
  default_args["start_date"] = datetime(2020, 2, 3)
elif environment == 'homo':
  default_args["start_date"] = datetime(2020, 2, 1)
elif environment == 'prod':
  default_args["start_date"] = datetime(2020, 2, 2)
else:
  raise ValueError('Wrong environment name')

dag = DAG("shipp_fee", default_args=default_args,
user_defined_macros=dict(env=environment),
schedule_interval=timedelta(5))

START = DummyOperator(
    task_id='Start',
    dag=dag
)

END = DummyOperator(
    task_id='End',
    dag=dag
)


t1 = DataValidation(
  task_id='DataValidation_pedidos',
  table_name='pedidos',
  s3_path='s3://ml-zaitt/'+environment+'/raw_metadata/',
  last_run_time="{{ prev_ds }}",
  current_run_time="{{ ds }}",
  start_date=default_args['start_date'],
  mysql_conn_id='shipp_mysql',
  wait_for_downstream=True,
  columns_name='id,cliente_id,created_at,updated_at,loja_id,estado,tempo_preparo,aproved_at,endereco_cliente_id,taxa_entrega,taxa_entrega_cliente,valor,motivo_recusa,comissao,reverso,score,tipo,taxa_entrega_payout,modo_entrega,e_critico,com_pedagio,motivo_id,cancelado_por,taxa_base,canceled_at,taxa_bonus,cliente_nao_recebeu,distance,time_shift',
  dag=dag
)
t2 = DataValidation(
  task_id='DataValidation_tentativas_aceite',
  table_name='tentativas_aceite',
  s3_path='s3://ml-zaitt/'+environment+'/raw_metadata/',
  last_run_time="{{ prev_ds }}",
  current_run_time="{{ ds }}",
  start_date=default_args['start_date'],
  mysql_conn_id='shipp_mysql',
  wait_for_downstream=True,
  columns_name='id,entregador_id,entrega_id,favor_id,latitude,longitude,descricao,created_at,updated_at',
  dag=dag
)
t3 = DataValidation(
  task_id='DataValidation_entregas',
  table_name='entregas',
  s3_path='s3://ml-zaitt/'+environment+'/raw_metadata/',
  last_run_time="{{ prev_ds }}",
  current_run_time="{{ ds }}",
  start_date=default_args['start_date'],
  mysql_conn_id='shipp_mysql',
  wait_for_downstream=True,
  columns_name='id,pedido_id,entregador_id,estado,created_at,updated_at,accepted_at_geolog_id,started_at_geolog_id,finished_at_geolog_id,multiplicador',
  dag=dag
)


t4 = DataValidation(
  task_id='DataValidation_solicitacoes_entrega',
  table_name='solicitacoes_entrega',
  s3_path='s3://ml-zaitt/'+environment+'/raw_metadata/',
  last_run_time="{{ prev_ds }}",
  current_run_time="{{ ds }}",
  start_date=default_args['start_date'],
  mysql_conn_id='shipp_mysql',
  wait_for_downstream=True,
  columns_name='id,entregador_id,entrega_id,created_at,updated_at',
  dag=dag
)


t5 = DataValidation(
  task_id='DataValidation_pedidos_rejeitados',
  table_name='pedidos_rejeitados',
  s3_path='s3://ml-zaitt/'+environment+'/raw_metadata/',
  last_run_time="{{ prev_ds }}",
  current_run_time="{{ ds }}",
  start_date=default_args['start_date'],
  mysql_conn_id='shipp_mysql',
  wait_for_downstream=True,
  columns_name='id,entregador_id,pedido_id,created_at,updated_at',
  dag=dag
)

t6 = DataValidation(
  task_id='DataValidation_enderecos',
  table_name='enderecos',
  s3_path='s3://ml-zaitt/'+environment+'/raw_metadata/',
  last_run_time="{{ prev_ds }}",
  current_run_time="{{ ds }}",
  start_date=default_args['start_date'],
  mysql_conn_id='shipp_mysql',
  wait_for_downstream=True,
  columns_name='id,endereco,numero,bairro,cidade,cep,complemento,latitude,longitude,loja_id,cliente_id,created_at,updated_at,localizacao,tipo',
  dag=dag
)


t7 = DataValidation(
  task_id='DataValidation_entregadores',
  table_name='entregadores',
  s3_path='s3://ml-zaitt/'+environment+'/raw_metadata/',
  last_run_time="{{ prev_ds }}",
  current_run_time="{{ ds }}",
  start_date=default_args['start_date'],
  mysql_conn_id='shipp_mysql',
  wait_for_downstream=True,
  columns_name='id,user_id,carteira_motorista,documento_veiculo,rg,codigo_promocional,tipo_veiculo,estado,created_at,updated_at,entregas_recebidas,entregas_aceitas,entregas_rejeitadas,data_ultimo_log,tempo_online,latitude_ultimo_log,longitude_ultimo_log,distancia_percorrida,banido,bag,regiao,activated_at',
  dag=dag
)

t8 = And(
  task_id='And',
  wait_for_downstream=True,
  dag=dag
) 

t9 = MySqlExtractor(
  task_id='MySql_Extractor_pedidos',
  mysql_conn_id='shipp_mysql',
  table_name='pedidos', 
  columns_name='id,cliente_id,created_at,updated_at,loja_id,estado,tempo_preparo,aproved_at,endereco_cliente_id,taxa_entrega,taxa_entrega_cliente,valor,motivo_recusa,comissao,reverso,score,tipo,taxa_entrega_payout,modo_entrega,e_critico,com_pedagio,motivo_id,cancelado_por,taxa_base,canceled_at,taxa_bonus,cliente_nao_recebeu,distance,time_shift',
  s3_path='s3://ml-zaitt/'+environment,
  current_run_time="{{ ds }}",
  last_run_time= "{{ prev_ds }}",
  wait_for_downstream=True,
  dag=dag
)
t10 = MySqlExtractor(
  task_id='MySql_Extractor_tentativas_aceite',
  mysql_conn_id='shipp_mysql',
  table_name='tentativas_aceite', 
  columns_name='id,entregador_id,entrega_id,favor_id,latitude,longitude,descricao,created_at,updated_at',
  s3_path='s3://ml-zaitt/'+environment,
  current_run_time="{{ ds }}",
  last_run_time= "{{ prev_ds }}",
  wait_for_downstream=True,
  dag=dag
)
t11 = MySqlExtractor(
  task_id='MySql_Extractor_entregas',
  mysql_conn_id='shipp_mysql',
  table_name='entregas', 
  columns_name='id,pedido_id,entregador_id,estado,created_at,updated_at,accepted_at_geolog_id,started_at_geolog_id,finished_at_geolog_id,multiplicador',
  s3_path='s3://ml-zaitt/'+environment,
  current_run_time="{{ ds }}",
  last_run_time= "{{ prev_ds }}",
  wait_for_downstream=True,
  dag=dag
)
t12 = MySqlExtractor(
  task_id='MySql_Extractor_solicitacoes_entrega',
  mysql_conn_id='shipp_mysql',
  table_name='solicitacoes_entrega', 
  columns_name='id,entregador_id,entrega_id,created_at,updated_at',
  s3_path='s3://ml-zaitt/'+environment,
  current_run_time="{{ ds }}",
  last_run_time= "{{ prev_ds }}",
  wait_for_downstream=True,
  dag=dag
)
t13 = MySqlExtractor(
  task_id='MySql_Extractor_pedidos_rejeitados',
  mysql_conn_id='shipp_mysql',
  table_name='pedidos_rejeitados', 
  columns_name='id,entregador_id,pedido_id,created_at,updated_at',
  s3_path='s3://ml-zaitt/'+environment,
  current_run_time="{{ ds }}",
  last_run_time= "{{ prev_ds }}",
  wait_for_downstream=True,
  dag=dag
)
t14 = MySqlExtractor(
  task_id='MySql_Extractor_enderecos',
  mysql_conn_id='shipp_mysql',
  table_name='enderecos', 
  columns_name='id,endereco,numero,bairro,cidade,cep,complemento,latitude,longitude,loja_id,cliente_id,created_at,updated_at,localizacao,tipo',
  s3_path='s3://ml-zaitt/'+environment,
  current_run_time="{{ ds }}",
  last_run_time= "{{ prev_ds }}",
  wait_for_downstream=True,
  dag=dag
)
t15 = MySqlExtractor(
  task_id='MySql_Extractor_entregadores',
  mysql_conn_id='shipp_mysql',
  table_name='entregadores', 
  columns_name='id,user_id,carteira_motorista,documento_veiculo,rg,codigo_promocional,tipo_veiculo,estado,created_at,updated_at,entregas_recebidas,entregas_aceitas,entregas_rejeitadas,data_ultimo_log,tempo_online,latitude_ultimo_log,longitude_ultimo_log,distancia_percorrida,banido,bag,regiao,activated_at',
  s3_path='s3://ml-zaitt/'+environment,
  current_run_time="{{ ds }}",
  last_run_time= "{{ prev_ds }}",
  wait_for_downstream=True,
  dag=dag
)





START >> [t1,t2,t3,t4,t5,t6,t7] >> t8 >> [t9,t10,t11,t12,t13,t14,t15] >> END
