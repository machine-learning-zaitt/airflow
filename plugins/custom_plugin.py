import logging
from airflow.models import BaseOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.hooks.S3_hook import S3Hook
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults
from airflow.exceptions import AirflowException
import pandas as pd
import pyarrow as pq
import re
import json

log = logging.getLogger(__name__)

class PostgresExtractor(BaseOperator):
    template_fields = ('run_time', 'last_run_time')

    @apply_defaults
    def __init__(self, sql_conn_id, schema, query, run_time, last_run_time, date_field='created_at', *args, **kwargs):
        self.query = query
        self.schema = schema
        self.sql_conn_id = sql_conn_id
        self.run_time = run_time
        self.last_run_time = last_run_time
        self.date_field = date_field      
        super(PostgresExtractor, self).__init__(*args, **kwargs)
    
    def execute(self, context):
        pg_hook = PostgresHook(postgres_conn_id=self.sql_conn_id, schema=self.schema)
        table_name = self.query.split('.')[-1]
        cursor = pg_hook.get_conn().cursor()
        query_with_between = self.query+" where "+self.date_field+" between '"+self.last_run_time+"' and '"+self.run_time+"';"
        logging.info('Query: '+ query_with_between)
        logging.info('Table name: '+ table_name)
        cursor.execute(query_with_between)
        sql_data = cursor.fetchall()
        logging.info('Data: ')
        logging.info(sql_data)
        logging.info('Getting column names and types')
        cursor.execute("select column_name,data_type from information_schema.columns where table_name = '"+table_name+"';")
        columns_data = cursor.fetchall()       
        columns_name_type = [(name_and_type[0],name_and_type[1]) for name_and_type in columns_data]
        columns_name = [name[0] for name in columns_data]
        logging.info('Columns names_type:')
        logging.info(columns_name_type)
        return {'data': [pd.DataFrame(sql_data, columns=columns_name).to_json()], 'columns_name_type':[columns_name_type], 'table_name':[table_name], 'run_time':[self.run_time]}

class S3DataWriter(BaseOperator):
    template_fields = ['key']
    @apply_defaults
    def __init__(self, key, metaData_name, *args, **kwargs):
        self.key=key
        self.metaData_name = metaData_name
        super(S3DataWriter, self).__init__(*args, **kwargs)
    
    def execute(self, context):
        s3 = S3Hook() # conn_id = aws_default
        s3.aws_conn_id = 's3_conn'
        # task_id = next(iter(self.upstream_task_ids))
        task_ids = [id for id in self.upstream_task_ids]
        # print(task_ids)
        logging.info('Pai: ')
        logging.info(str(task_ids))
        values = []
        table_names = []
        dates = []
        valueStrings = []
        for task_id in task_ids:
            values.append(context['task_instance'].xcom_pull(task_ids=task_id))
            table_names.append(context['task_instance'].xcom_pull(task_ids=task_id)['table_name'])
            dates.append(context['task_instance'].xcom_pull(task_ids=task_id)['run_time'])
            valueStrings.append(context['task_instance'].xcom_pull(task_ids=task_id)[self.metaData_name])
        logging.info('Table name: '+ str(table_names))
        print(str(valueStrings))        
        if self.metaData_name == 'data':
            for i, valueString in enumerate(valueStrings):
                print(valueString)
                for j, valueStrin in enumerate(valueString):
                    print(valueStrin)
                    tempValueString = pd.read_json(valueStrin[0])      
                    tempValueString.to_parquet('temp.parquet',allow_truncated_timestamps=True)       
                    path=self.key+'raw_data/'+table_names[i][j][0]+'/'+'date_batch='+dates[i][j][0]+'/'+dates[i][j][0]+'_'+table_names[i][j][0]+'.parquet'
                    logging.info('Writing data on parquet on path:')
                    logging.info(path)    
                    s3.load_file(filename='temp.parquet', key=path, replace=True)
        else:
            json_dict = {}
            keys = []
            for i, valueString in enumerate(valueStrings):
                print(valueString)
                for j, valueStrin in enumerate(valueString):
                    print(valueStrin)
                    key=self.key+'raw_metadata/'+table_names[i][j]+'/'+'date_batch='+dates[i][j]+'/'+dates[i][j]+'_'+table_names[i][j]+'.json'
                    json_dict = {}
                    for item in valueStrin:
                        json_dict[item[0]] = item[1]
                    json_obj = json.dumps(json_dict)
                    print('Wrinting this json:')
                    print(json_obj)
                    with open('json_obj.json', 'w') as f:
                        f.write(json_obj)
                    logging.info('Writing json on path:')
                    logging.info(key)
                    s3.load_file(filename='json_obj.json', key=key, replace=True)
                keys.append(key)
            print(keys)
            return {'path': keys}

class Verifier(BaseOperator):
    template_fields = ['last_run_time']

    @apply_defaults
    def __init__(self, last_run_time, start_date, *args, **kwargs):
        self.task_id = kwargs.get('task_id')
        self.last_run_time=last_run_time
        self.start_date=start_date
        super(Verifier, self).__init__(*args, **kwargs)
    
    def execute(self, context):
        s3 = S3Hook() # conn_id = aws_default
        s3.aws_conn_id = 's3_conn'
        task_id = next(iter(self.upstream_task_ids))
        logging.info('Pai: ')
        logging.info(task_id)
        value =context['task_instance'].xcom_pull(task_ids=task_id)      
        recent_paths = value['path']
        recent_paths_dict = {}
        recent_s3_obects_dict = {}
        last_batch_paths_dict = {}
        table_sufixes = []
        for recent_path in recent_paths:
            key = recent_path.split('/')[5]
            recent_paths_dict[key]= recent_path
            recent_s3_obects_dict[key] = s3.read_key(recent_path)
            last_batch_paths_dict[key] = re.sub('\d+-\d+-\d+', self.last_run_time, recent_path)
            table_sufixes.append(key)
        for key, value in last_batch_paths_dict.items():
            if not s3.check_for_key(value):
                logging.info('First time json!')
                postgresDatas = []
                postgresMetas = []
                postgresTablenames = []
                postgresRuntimes = []
                for table_sufix in table_sufixes:
                    postgresValues = context['task_instance'].xcom_pull(task_ids='Postgres_extractor'+'_'+table_sufix)
                    postgresDatas.append(postgresValues['data'])
                    postgresMetas.append(postgresValues['columns_name_type'])
                    postgresTablenames.append(postgresValues['table_name'])
                    postgresRuntimes.append(postgresValues['run_time'])
                return {'data': postgresDatas, 'columns_name_type':postgresMetas, 'table_name':postgresTablenames, 'run_time':postgresRuntimes}
            else:
                logging.info('Path old json: ')
                logging.info(last_batch_paths_dict[key])
                logging.info('Path recent json: ')
                logging.info(recent_paths_dict[key])
                last_batch_s3_object = s3.read_key(last_batch_paths_dict[key])
                recent_batch_s3_object = s3.read_key(recent_paths_dict[key])
                recent_json = json.loads(recent_batch_s3_object)
                last_json = json.loads(last_batch_s3_object)
                logging.info("JSON_NEW: ")
                logging.info(recent_json)
                logging.info("LEN")
                logging.info(len(recent_json))
                logging.info("JSON_OLD: ")
                logging.info(last_json)
                logging.info("LEN")
                logging.info(len(last_json))
                equal = True
                if set(recent_json.keys()) == set(last_json.keys()):
                    for key in recent_json.keys():
                        if key in last_json:
                            if recent_json[key] != last_json[key]:
                                equal = False
                                break
                    if equal:
                        postgresDatas = []
                        postgresMetas = []
                        postgresTablenames = []
                        postgresRuntimes = []
                        for table_sufix in table_sufixes:
                            postgresValues = context['task_instance'].xcom_pull(task_ids='Postgres_extractor'+'_'+table_sufix)
                            postgresDatas.append(postgresValues['data'])
                            postgresMetas.append(postgresValues['columns_name_type'])
                            postgresTablenames.append(postgresValues['table_name'])
                            postgresRuntimes.append(postgresValues['run_time'])
                        return {'data': postgresDatas, 'columns_name_type':postgresMetas, 'table_name':postgresTablenames, 'run_time':postgresRuntimes}
                else:
                    raise ValueError('Columns name or types are not the same')
        
            

class MyFirstPlugin(AirflowPlugin):
    name = "my_first_plugin"
    operators  = [PostgresExtractor, S3DataWriter, Verifier]