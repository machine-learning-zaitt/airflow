import logging
from airflow.models import BaseOperator
from airflow.hooks.mysql_hook import MySqlHook
from airflow.hooks.S3_hook import S3Hook
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults
from airflow.exceptions import AirflowException
import pandas as pd
import pyarrow as pq
import re
import json
from datetime import datetime

log = logging.getLogger(__name__)

class MySqlExtractor(BaseOperator):
    template_fields = ('current_run_time', 'last_run_time')

    @apply_defaults
    def __init__(self, table_name, s3_path, columns_name, mysql_conn_id, current_run_time, last_run_time, date_field='created_at', *args, **kwargs):
        self.current_run_time = current_run_time
        self.s3_path = s3_path
        self.last_run_time = last_run_time
        self.date_field = date_field    
        self.table_name = table_name
        self.columns_name = columns_name
        self.mysql_conn_id = mysql_conn_id
        super(MySqlExtractor, self).__init__(*args, **kwargs)
    
    def execute(self, context):
        s3 = S3Hook() 
        s3.aws_conn_id = 's3_conn'
        mysql = MySqlHook(mysql_conn_id=self.mysql_conn_id).get_conn()
        cursor = mysql.cursor()
        ## Metadata Extraction
        cursor.execute('SHOW COLUMNS FROM ebdb.'+self.table_name+';')    
        tempJsonTable = {self.table_name : "fieldTable" }
        fields= {}
        for result in cursor:
               fields[result[0]] = {"type" : result[1],
                                    "null" : result[2],
                                    "key" : result[3],
                                    "default" : ("NULL" if result[4] == None else result[4]),
                                    "extra" : result[5]
               }                    
        tempJsonTable[self.table_name] = fields
        key = self.s3_path+'/raw_metadata/'+self.table_name+'/'+'date_batch='+self.current_run_time+'/'+self.current_run_time+'_'+self.table_name+'.json'
        with open(self.table_name+'.json', 'w') as f:
                    f.write(str(tempJsonTable))
        logging.info('Writing json on path:')
        logging.info(key)
        s3.load_file(filename=self.table_name+'.json', key=key, replace=True)
        ## Data Extraction
        queryString = "SELECT "+self.columns_name+" FROM ebdb."+self.table_name+" where created_at between '" + self.last_run_time + "' and '" + self.current_run_time+ "'"
        cursor.execute(queryString)
        dataframe = pd.DataFrame(cursor.fetchall(), columns=self.columns_name.split(','))
        print(dataframe.dtypes)
        dataframe.to_parquet('temp.parquet',allow_truncated_timestamps=True)
        path = self.s3_path+'/raw_data/'+self.table_name+'/'+'date_batch='+self.current_run_time+'/'+self.current_run_time+'_'+self.table_name+'.parquet'
        logging.info('Writing data on parquet on path:')
        logging.info(path)    
        s3.load_file(filename='temp.parquet', key=path, replace=True)
        return True

def clearEnum(stringClear):
    """ clears the first double quote inside the enum """
    s = stringClear
    if(s.index("enum(\"")):
        sbegin= s.index("enum(\"")
        substri= s[sbegin:]
        sEnd = substri.index(")")
        sEnum=s[sbegin:sEnd+sbegin+1]
        cEnum= sEnum.replace("\""," ")
        newString= s[:sbegin]+cEnum+s[sEnd+1-len(substri):]
    return newString

def ClearEnumS3(s):
    """ clears all double quotes from a string """
    a= True
    while a :
        try:
            if(s.index("enum(\"")):
                s = clearEnum(s)   
        except:
            a = False
    return s


class DataValidation(BaseOperator):
    template_fields = ['last_run_time', 'current_run_time']

    @apply_defaults
    def __init__(self, mysql_conn_id, table_name, columns_name, s3_path, last_run_time, current_run_time, start_date, *args, **kwargs):
        self.task_id = kwargs.get('task_id')
        self.last_run_time=last_run_time
        self.start_date=start_date
        self.table_name=table_name
        self.s3_path=s3_path
        self.columns_name=columns_name
        self.current_run_time=current_run_time
        self.mysql_conn_id = mysql_conn_id
        super(DataValidation, self).__init__(*args, **kwargs)
    
    def execute(self, context):
        s3 = S3Hook() # conn_id = aws_default
        s3.aws_conn_id = 's3_conn'
        fmt = '%Y-%m-%dT%H:%M:%S%z'
        string_date = self.start_date.strftime(fmt) 
        maskStart_date = string_date[0:10]
        if datetime.strptime(self.last_run_time, "%Y-%m-%d").date() <  datetime.strptime(maskStart_date, "%Y-%m-%d").date():
            logging.info('First run')
            return True
        else:   
            key = self.s3_path+self.table_name+'/'+'date_batch='+self.last_run_time+'/'+self.last_run_time+'_'+self.table_name+'.json'
            strS3Json = s3.read_key(key)
            mysql = MySqlHook(mysql_conn_id=self.mysql_conn_id).get_conn()
            cursor = mysql.cursor()
            strS3Json = strS3Json.replace("\'","\"")
            s3Json = json.loads(ClearEnumS3(strS3Json)) 
            cursor.execute('SHOW COLUMNS FROM ebdb.'+self.table_name+';')
            tempJsonTable = {self.table_name : "fieldTable" }
            fields= {}  
            for result in cursor:
                fields[result[0]] = {"type" : result[1],
                                     "null" : result[2],
                                     "key" : result[3],
                                     "default" : ("NULL" if result[4] == None else result[4]),
                                     "extra" : result[5]
                                        
                }  
            
            tempJsonTable[self.table_name] = fields
            strTempJsonTable = str(tempJsonTable).replace("\'","\"")
            tempJsonTable = json.loads(ClearEnumS3(strTempJsonTable)) 
            if (strTempJsonTable == strS3Json):
                logging.info('There was no change in the tables!')
                return True
            else:
                logging.info('There was a change in the tables!')    
                for (k, v) in s3Json.items():
                    for (j,l) in v.items():
                        for m,n in l.items():
                            if ((tempJsonTable[k][j][m])==n):                               
                                logging.info(str(j)+"Field:"+str(m)+" Value:"+str(n))
                                logging.info("ok")
                            else:
                                logging.info(str(j)+"ERROR! Failed in the field: "+str(m)+":"+str(n))
                                return False                 
class And(BaseOperator):
    
    @apply_defaults
    def __init__(self, *args, **kwargs):      
        super(And, self).__init__(*args, **kwargs)
    
    def execute(self, context):
        task_ids = [id for id in self.upstream_task_ids]
        values = []
        for task_id in task_ids:
            values.append(context['task_instance'].xcom_pull(task_ids=task_id))
        if False in values:
            raise Exception("Some value has been changed!")
        else:
            return True
                     
class shipp_fee_plugin(AirflowPlugin):
    name = "shipp_fee_plugin"
    operators  = [MySqlExtractor, DataValidation, And]