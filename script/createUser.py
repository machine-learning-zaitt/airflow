import airflow
from airflow import models, settings
from airflow.contrib.auth.backends.password_auth import PasswordUser
import sys

user = PasswordUser(models.User())
user.username = sys.argv[1]
user.email = 'change@it.com.br'
user.password = sys.argv[2]
user.superuser = sys.argv[3] == 'True'
session = settings.Session()
session.add(user)
session.commit()
session.close()
