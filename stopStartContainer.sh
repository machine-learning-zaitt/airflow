#!/bin/bash

containersIDs=$(docker ps -a -q)
imagesIDs=$(docker images -a -q)

if [ -z "$containersIDs"]
then
    echo 'No container running'
else
    docker stop $containersIDs
    docker rm $containersIDs
fi
if [ -z "$imagesIDs"]
then
    echo 'No image'
else
    docker rmi $imagesIDs
fi
